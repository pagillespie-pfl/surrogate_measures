import subprocess
import scipy.io.wavfile

def read_wavfile(filename):
    fs, wav = scipy.io.wavfile.read(filename)
    return fs, wav

def free_up_space(filename):
    """Tells onedrive to Free Up Space and keep the file only in the cloud."""
    subprocess.run('attrib +U -P "' + filename + '"')